Sainsbury Test App
==================

Introduction
------------
This is console application to crawl trough given link on Sainsbury website and perform parsing action on data

Installation
------------

Dependencies
------------
As crawling trough given website was very tricky (javascript interpretation has to be enabled on crawler) 
I am using mink-zombie driver for Behat for this job. In this case dependencies are:

* nodejs in newest version
* npm in newest version
* zombie npm module installed global
* ENV VARIABLE pointing for global node_modules folder
* Other dependencies are served via composer
    * PHP >= 5.5
    * PHPUNIT <= 4.9
    * Mockery >= 0.9
    * php-simple-html-dom-parser >= 1.5.0
    * behat/mink -latest
    * behat/mink-zombie-driver -latest

Installation - Composer
-----------------------
The recommended way to get a working copy of this project is to clone the repository
and use `composer` to install basic dependencies:
    
    cd my/project/dir
    php composer.phar self-update
    php composer.phar install

Next ensure yourself that you have `nodejs`, `npm` and `zombie` installed.
To install `zombie` follow:

    http://mink.behat.org/en/latest/drivers/zombie.html

Running the application
-----------------------

    cd my/project/dir
    php public/index.php scrap
    
Running tests
-------------
    cd my/project/dir/[..]/module/Application/test
    phpunit
    
Author NOTE
-----------
Please be aware that not full test coverage is done as whoel task took more than 2hours.