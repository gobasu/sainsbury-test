<?php

return [
    'console' => [
        'router' => [
            'routes' => [
                'scrap' => [
                    'options' => [
                        'route' => 'scrap',
                        'defaults' => [
                            'controller' => Application\Controller\IndexController::class,
                            'action' => 'scrap',
                        ],
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'invokables' => [
            Application\Controller\IndexController::class => Application\Controller\IndexController::class,
        ],
    ],
    'service_manager' => [
        'invokables' => [
            Application\Mapper\WebsiteMapper::class => Application\Mapper\WebsiteMapper::class,
            Application\Service\WebSiteClientService::class => Application\Service\WebSiteClientService::class,
        ],
        'factories' => [
            \Application\Factory\WebsiteFactory::class => function (\Zend\ServiceManager\ServiceLocatorInterface $sm) {
                $factory = new Application\Factory\WebsiteFactory(
                    $sm->get('Config')['addresses_to_scrap'],
                    $sm->get(\Application\Service\WebSiteClientService::class),
                    $sm->get(\Application\Mapper\WebsiteMapper::class)
                );
                return $factory;
            }
        ],
    ],
];
