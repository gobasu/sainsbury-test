<?php

namespace Application\Controller;

use Application\Factory\WebsiteFactory;
use Application\Mapper\WebsiteMapper;
use Zend\Mvc\Controller\AbstractConsoleController;

/**
 * Class IndexController
 * @package Application\Controller
 */
class IndexController extends AbstractConsoleController
{
    public function scrapAction()
    {
        //Get Factory and Mapper
        $mapper = $this->getServiceLocator()->get(WebsiteMapper::class);
        $factory = $this->getServiceLocator()->get(WebsiteFactory::class);

        $results = [];
        //Loop trough websites and find products
        foreach ($factory->getObject() as $dom) {
            $results[] = array_merge(
                $mapper->parseProducts($dom),
                [
                    //get dom size and save it as kb
                    'size' => (int)(mb_strlen($dom, '8bit') / 1024) .'kb'
                ]
            );
        }
        //encode array and return to output
        return json_encode(['results' => $results]);
    }
}
