<?php

namespace Application\Factory;

use Application\Mapper\WebsiteMapperInterface;
use Application\Service\WebSiteClientServiceInterface;
use Sunra\PhpSimple\HtmlDomParser;

/**
 * Class WebsiteFactory
 * @package Application\Factory
 */
class WebsiteFactory
{
    /**
     * @var array
     */
    private $urls = [];

    /**
     * @var WebSiteClientServiceInterface
     */
    private $client;

    /**
     * @param string $url
     * @param WebSiteClientServiceInterface $client
     * @param WebsiteMapperInterface $mapper
     */
    public function __construct($url, WebSiteClientServiceInterface $client, WebsiteMapperInterface $mapper)
    {
        $this->client = $client;
        $body = $this->client->getWebsite($url);
        $dom = HtmlDomParser::str_get_html($body);
        $this->urls = $mapper->leechUrls($dom);
    }

    /**
     * @return \Generator
     */
    public function getObject()
    {
        foreach ($this->urls as $url) {
            $body = $this->client->getWebsite($url);
            $dom = HtmlDomParser::str_get_html($body);
            yield $dom;
        }
    }
}
