<?php

namespace Application\Mapper;

/**
 * Class SainsburyWebsiteMapper
 * @package Application\Mapper
 */
class WebsiteMapper implements WebsiteMapperInterface
{

    /**
     * @param \simple_html_dom $dom
     * @return array
     */
    public function leechUrls(\simple_html_dom $dom)
    {
        $hrefs = [];
        $elements = $dom->find('body #page #main #content #productsContainer #productLister ul.productLister li');
        foreach ($elements as $li) {
            $hrefs[] = trim($li->find('.product .productInner .productInfoWrapper .productInfo h3 a')[0]->href);
        }
        return $hrefs;
    }

    /**
     * Mapp
     * @param \simple_html_dom $dom
     * @return array
     */
    public function parseProducts(\simple_html_dom $dom)
    {
        return [
            'title' => trim($dom->find('.productTitleDescriptionContainer h1')[0]->plaintext),
            'unit_price' => explode('/', ltrim(trim($dom->find('p.pricePerUnit')[0]->plaintext), '£'))[0],
            'description' => trim($dom->find('.productText p')[0]->plaintext),
        ];
    }
}
