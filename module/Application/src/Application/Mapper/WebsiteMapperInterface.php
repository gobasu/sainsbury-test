<?php

namespace Application\Mapper;

interface WebsiteMapperInterface
{
    public function leechUrls(\simple_html_dom $dom);
    public function parseProducts(\simple_html_dom $dom);
}
