<?php

namespace Application\Service;

/**
 * Class WebSiteClientService
 * @package Application\Service
 */
class WebSiteClientService implements WebSiteClientServiceInterface
{

    /**
     * @var \Behat\Mink\Session
     */
    private $session;

    /**
     * @see http://mink.behat.org/en/latest/drivers/zombie.html
     */
    public function __construct()
    {
        $driver = new \Behat\Mink\Driver\ZombieDriver(
            new \Behat\Mink\Driver\NodeJS\Server\ZombieServer()
        );
        $this->session = new \Behat\Mink\Session($driver);
        $this->session->start();
    }

    /**
     * @param $url
     * @return string
     */
    public function getWebsite($url)
    {
        $this->session->visit($url);
        $page = $this->session->getPage();
        return $page->getContent();
    }
}
