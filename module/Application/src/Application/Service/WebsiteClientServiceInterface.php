<?php

namespace Application\Service;

interface WebSiteClientServiceInterface
{
    public function getWebsite($url);
}
