<?php

namespace ApplicationTest\Controller;

use Mockery\Adapter\Phpunit\MockeryPHPUnitIntegration;
use Zend\Test\PHPUnit\Controller\AbstractConsoleControllerTestCase;

/**
 * Class ApplicationControllerTest
 * @package ApplicationTest\Controller
 */
class ApplicationControllerTest extends AbstractConsoleControllerTestCase
{
    use MockeryPHPUnitIntegration;

    public function setUp()
    {
        $this->setApplicationConfig(
            include __DIR__ . '/../../../../../config/application.config.php'
        );
        parent::setUp();
    }

    public function testControllerBasics()
    {
        $this->dispatch('scrap');
        $this->assertResponseStatusCode(0);

        $this->assertModuleName('Application');
        $this->assertControllerName(\Application\Controller\IndexController::class);
        $this->assertControllerClass('IndexController');
        $this->assertMatchedRouteName('scrap');
        $this->assertJson($this->getActualOutput());
    }
}
