<?php

namespace ApplicationTest\Factory;

use Application\Factory\WebsiteFactory;
use Application\Mapper\WebsiteMapper;
use Application\Service\WebSiteClientService;

/**
 * Class WebsiteFactoryTest
 * @package ApplicationTest\Factory
 */
class WebsiteFactoryTest extends \PHPUnit_Framework_TestCase
{
    public function testFactory()
    {
        $url = 'http://fake.com';
        $body = "<html><body><fake>dsadas</fake></body></html>";
        $client = \Mockery::mock(WebSiteClientService::class);
        $client->shouldReceive('getWebsite')->once()->with($url)->andReturn($body);
        $mapper = \Mockery::mock(WebsiteMapper::class);
        $mapper->shouldReceive('leechUrls')->once()
            ->andReturn(['http://fake2.com']);
        $factory = new WebsiteFactory($url, $client, $mapper);
        $this->assertAttributeEquals(['http://fake2.com'], 'urls', $factory);

        $object = $factory->getObject();
        $this->assertInstanceOf('Generator', $object);
    }
}
