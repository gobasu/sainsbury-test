<?php

namespace ApplicationTest\Mapper;
use Application\Mapper\WebsiteMapper;
use Sunra\PhpSimple\HtmlDomParser;

/**
 * Class WebsiteMapperTest
 * @package ApplicationTest\Mapper
 */
class WebsiteMapperTest extends \PHPUnit_Framework_TestCase
{

    public function testValidHtmlLeechUrls()
    {
        $valid = '<body><div id="page"><div id="main"><div id="content"><div id="productsContainer">'
            .'<div id="productLister"><ul class="productLister"><li><div class="product">'
            .'<div class="productInner"><div class="productInfoWrapper"><div class="productInfo">'
            .'<h3><a href="http://fake.com"></a></h3>'
            .'</div></div></div>'
            .'</div></li><li><div class="product">'
            .'<div class="productInner"><div class="productInfoWrapper"><div class="productInfo">'
            .'<h3><a href="http://fake2.com"></a></h3>'
            .'</div></div></div>'
            .'</div></li></ul></div>'
            .'</div></div></div></div></body>';
        $mapper = new WebsiteMapper();
        $hrefs = $mapper->leechUrls(HtmlDomParser::str_get_html($valid));
        $this->assertEquals(['http://fake.com', 'http://fake2.com'], $hrefs);
    }

    public function testInvalidHtmlLeechUrls()
    {
        $valid = '<body><div id="psadage"><div id="main"><div id="content"><div id="productsContainer">'
            .'<div id="productLister"><ul class="productLister"><li><div class="product">'
            .'<div class="productI43242nner"><div class="productInfoWrapper"><div class="productInfo">'
            .'<h3><a href="http://fake.asdascom"></a></h3>'
            .'</div></div></div>'
            .'</div></li><li><div class="product">'
            .'<div class="productInner"><div class="prodasdaductInfoWrapper"><div class="productInfo">'
            .'<h3><a href="http:/asd/safake2.com"></a></h3>'
            .'</div></div></div>'
            .'</div></li></ul></div>'
            .'</div></div></div></div></body>';
        $mapper = new WebsiteMapper();
        $hrefs = $mapper->leechUrls(HtmlDomParser::str_get_html($valid));
        $this->assertEmpty($hrefs);
    }
}
